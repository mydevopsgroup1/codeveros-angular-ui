exports.handler = (event, context, callback) => {

    const response = {
        statusCode: 200,
        body: JSON.stringify("Hello LAMBDA!  " +event.multiValueHeaders["User-Agent"]),
    };
    callback(null, response);
};

