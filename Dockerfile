FROM nginx:1.15-alpine

COPY dist/codeveros-core-ui /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
