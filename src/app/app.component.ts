import { Injectable, Component, OnInit, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'codeveros-root',
  templateUrl: './app.component.html'
})

@Injectable()
export class AppComponent implements OnInit {
    constructor(private http: HttpClient) { }
    restoutput: Observable<string>;
    // demo: url = {serverless url} goes here
    url = 'https://iqyfvyn6r4.execute-api.us-east-1.amazonaws.com/dev/microservice';

    ngOnInit() {
        this.restoutput = this.http.get<string>(this.url);
    }
}
